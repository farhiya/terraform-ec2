provider "aws" {
shared_credentials_files = ["/Users/Farhiya/Downloads/credentials"]
profile = "Academy"
region = "eu-west-3"
}

data "terraform_remote_state" "my-infrastructure" {
    backend = "s3" 
    config = {
        bucket = "farhiya-academy-terraform-1"  # For Academy account
        key    = "infra.tfstate-academy"
        region = "eu-west-3"
        shared_credentials_file = "/Users/Farhiya/Downloads/credentials"
        profile = "Academy"
    }
}

