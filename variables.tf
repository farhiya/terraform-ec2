variable "ubuntu_ami" {
	description = "ubuntu ami in Paris or my Packer AMI"
	# apache
	default = "ami-08df1f2e91225aad7"


	# nginx
	# default = "ami-05a1438f6a21c8089"
}

variable "my_tag" {
		description = "Name to tag all elements with"
		default = "farhiya"
}

variable "ec2_instance_type" {
	description = "Instance type of my server"
	default = "t2.micro"
}

variable "my_cidr" {
	description = "My IP address"
}

variable "aws_key_name" {
	description = "AWS Keyname for SSH"
	default = "farhiya-terraform-key-1"
}

