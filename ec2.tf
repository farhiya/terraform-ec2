resource "aws_security_group" "my-security-group" {
  name = "${var.my_tag}_server_group_public"
  vpc_id  = data.terraform_remote_state.my-infrastructure.outputs.vpc_id
}

resource "aws_security_group_rule" "allow_ssh_master" {
      security_group_id = aws_security_group.my-security-group.id
      type              = "ingress"
      from_port         = 22
      to_port           = 22
      protocol          = "tcp"
      cidr_blocks       = [var.my_cidr]
}
resource "aws_security_group_rule" "allow_http" {
     security_group_id = aws_security_group.my-security-group.id
     type              = "ingress"
     from_port         = 80
     to_port           = 80
     protocol          = "tcp"
     cidr_blocks       = [var.my_cidr]
}


resource "aws_security_group_rule" "outbound_master" {
       security_group_id = aws_security_group.my-security-group.id
       type              = "egress"
       from_port         = 0
       to_port           = 0
       protocol          = -1
       cidr_blocks       = ["0.0.0.0/0"]
}



/*
Public nginx web server
*/

resource "aws_instance" "my-public-nginx" {
  ami           = var.ubuntu_ami
  instance_type = var.ec2_instance_type
  key_name = var.aws_key_name
  subnet_id = data.terraform_remote_state.my-infrastructure.outputs.public_subnet_id_2
  vpc_security_group_ids = [aws_security_group.my-security-group.id]

  tags = {
    Name = "${var.my_tag}_public_nginx"
  }

  # user_data = data.template_file.user-data.rendered
 
}

# data "template_file" "user-data" {
#     template = file("ec2-server.conf")
# }



# resource "aws_instance" "web_instances" {
#   ami           = "ami-03ab7423a204da002"
#   instance_type = "t2.micro"

#   # user_data = data.template_file.init.rendered
#   tags = {
#     Name = "${var.my_tag}-web-launched-ec2"
#   }
# }


# data "template_file" "init" {
#     template = "init.sh.tpl"
#     # var = {
#     #     example_var = "123.2.2.1"
#     # }
# }
